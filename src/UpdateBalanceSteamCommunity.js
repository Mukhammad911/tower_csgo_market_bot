/**
 * Date: 02.06.2019
 * Author: Muhammad.Yakubov91@gmail.com
 * Description: This script syncronized price of item from market steamcommunity with local databases
 * App: 730 CSGO
 *
 */
var Db                  = require('./inc/Db.js');
var Logging             = require('./inc/Logging.js');
const market            = require('steam-market-pricing');

var db                  = new Db;
var logging             = Logging('UpdateBalanceSteamCommunity');

function UpdateBalanceSteamCommunity() {
    logging && console.log('[UpdateBalanceSteamCommunity]');
    this.products = [];
    this.get_products = [];
    setInterval(this.updateBalance.bind(this), 30000);
    this.updateBalance();
}

UpdateBalanceSteamCommunity.prototype.updateBalance = function () {
    logging && console.log('[UpdateBalanceSteamCommunity][updateBalance]');
    db.query('SELECT * FROM available_products WHERE updated_at <= NOW() - INTERVAL 10 DAY LIMIT 5', (function (err, result) {
        if (err) throw err;

        for (var result_index in result) {
            this.products.push(
                result[result_index].market_name
            )
        }
        logging && console.log('[UpdateBalanceSteamCommunity][updateBalance][length]: ' + this.products.length);
        if (!this.products.length) {
            return;
        }

        this.getPriceSteam();

    }).bind(this));
}

UpdateBalanceSteamCommunity.prototype.updateBalanceToDB = function(rub_price, market_hash_name, callback){
    logging && console.log('[UpdateBalanceSteamCommunity][updateBalanceToDB][item][market_hash_name]: ' + market_hash_name);
    logging && console.log('[UpdateBalanceSteamCommunity][updateBalanceToDB][item][rub_price]: ' + rub_price);
    db.query('\
             UPDATE `available_products` \
             SET \
             `price` = "' + rub_price + '", \
            `updated_at` = now() \
             WHERE `market_name` = "' + market_hash_name+ '"; \
              ', function (err, result3) {
                    if (err) throw err;
                    callback(rub_price, market_hash_name);
                }.bind(this));

}

UpdateBalanceSteamCommunity.prototype.getPriceSteam = function () {
    logging && console.log('[UpdateBalanceSteamCommunity][getPriceSteam]');
    market.getItemsPrices(730, this.products).then(items =>
    {
        console.log(items);
        this.products.forEach(name => {
            this.get_products.push({
               market_hash_name : name,
               price : items[name].lowest_price
            })
        })
        this.saveData();
    })
}

UpdateBalanceSteamCommunity.prototype.saveData = function()
{
    var runGetData = function(){
        logging && console.log('[UpdateBalanceSteamCommunity][saveData][runGetData]');

        logging && console.log('[UpdateBalanceSteamCommunity][saveData][length]: '+this.get_products.length);

        if(!this.get_products.length){
            logging && console.log('[UpdateBalanceSteamCommunity][saveData][length]: '+this.get_products.length);
            logging && console.log('[UpdateBalanceSteamCommunity][saveData][End]');
            this.products = [];
            this.get_products = [];
            return;
        }

        var product = this.get_products[0];

        this.get_products.splice(0,1);


        logging && console.log('[UpdateBalanceSteamCommunity][saveData][market_name]: ' + product.market_hash_name);

        logging && console.log('[UpdateBalanceSteamCommunity][saveData][item][lowest_price][steam]: ' + product.price);

        if(product.price == undefined || product.price == ""){
            runGetData();
        }

        if(product.price.charAt(0) === '$' ){
            product.price = product.price.slice(1);
        }

        //Convert to RUB

        var rub_price = parseInt(parseFloat(product.price) * 60 * 100);

        logging && console.log('[UpdateBalanceSteamCommunity][saveData][item][rub_price][price]: ' + rub_price);

        this.updateBalanceToDB(rub_price, product.market_hash_name, function(rub_price, market_hash_name){
            this.updateOrCreatePrice(rub_price,  market_hash_name, function(){
                runGetData();
            }.bind(this));
        }.bind(this))
    }.bind(this);

    runGetData();
}

UpdateBalanceSteamCommunity.prototype.updateOrCreatePrice= function(rub_price, market_hash_name, callback ){
    logging && console.log('[UpdateBalanceSteamCommunity][updateOrCreatePrice][market_hash_name]: ' + market_hash_name);
    logging && console.log('[UpdateBalanceSteamCommunity][updateOrCreatePrice][rub_price]: ' + rub_price);

    db.query('SELECT * FROM `product_csgo_price_steam` where `market_hash_name` =  "' + market_hash_name + '";',
        (function (err, result2) {
            if (err) throw err;

            if (result2.length) {

                db.query('\
                         UPDATE `product_csgo_price_steam` \
                         SET \
                         `price` = ' + rub_price + ', \
                        `updated_at` = now() \
                         WHERE `market_hash_name` = "' + market_hash_name + '"; \
                          ', function (err, result3) {
                        if (err) throw err;
                            callback();
                        }.bind(this));
            }
            else {
                db.query('INSERT INTO `product_csgo_price_steam`(`market_hash_name`, `price`,  `created_at`, `updated_at`) VALUES ("' + market_hash_name + '",' + rub_price + ',now(),now())',
                    function (err, result4) {
                        if (err) throw err;
                        callback();
                    }.bind(this));
            }
        }.bind(this)));
}

var updateBalanceSteamCommunity = new UpdateBalanceSteamCommunity();
