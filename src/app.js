require('console-stamp')(console, {
    pattern: 'yyyy-mm-dd HH:MM:ss.l',
    metadata: '[APP]',
    colors: {
        stamp: "yellow",
        label: "white",
        metadata: "green"
    }
});

var Redis      = require('ioredis');
var Config     = require('./inc/Config.js');
var BotManager = require('./inc/BotManager.js');

var redis      = new Redis;
var config     = new Config;
var botManager = new BotManager;

redis.subscribe(config.REDIS_PREFIX+'disable-bot', config.REDIS_PREFIX+'enable-bot', function(err, count) {
    if (err) throw err;
});

redis.on('message', function(channel, message) {
    var channelClear = (config.REDIS_PREFIX != '') ? channel.replace(config.REDIS_PREFIX, '') : channel;

    message = JSON.parse(message);

    switch(channel) {
        case config.REDIS_PREFIX+'disable-bot':
            botManager.disableBot(message.botId);
            break;
        case config.REDIS_PREFIX+'enable-bot':
            botManager.enableBot(message.botId);
            break;
    }
});

