var botId  = process.argv.slice(2)[0];

require('console-stamp')(console, {
    pattern: 'yyyy-mm-dd HH:MM:ss.l',
    metadata: '[BOT#' + botId + ']',
    colors: {
        stamp: "yellow",
        label: "white",
        metadata: "green"
    }
});

var Redis      = require('ioredis');
var Config     = require('./inc/Config.js');
var Bot        = require('./inc/Bot.js');

var redis      = new Redis;
var config     = new Config;
var bot        = new Bot(botId);

var proxy = config.PROXIES.split(',')[parseInt(Math.random() * config.PROXIES.split(',').length)];
if (proxy) {
    process.env.HTTP_PROXY = 'http://' + proxy;
    process.env.HTTPS_PROXY = 'http://' + proxy;
}

redis.subscribe(
    config.REDIS_PREFIX+'csgo-purchase-change-'+ botId,
    config.REDIS_PREFIX+'send-drop-'+ botId,
    config.REDIS_PREFIX+'purchase-change-'+ botId,
    config.REDIS_PREFIX+'widthdraw-csgo-'+botId,
    config.REDIS_PREFIX+'once-free-'+ botId,
    function(err, count) {
        if (err) throw err;
    });

redis.on('message', function(channel, message) {
    var channelClear = (config.REDIS_PREFIX != '') ? channel.replace(config.REDIS_PREFIX, '') : channel;

    message = JSON.parse(message);

    switch (channel) {
        case config.REDIS_PREFIX+'send-drop-'+botId:
            bot.sendDrop(message.accountId, message.token, message.drop);
            break;
        case config.REDIS_PREFIX+'purchase-change-'+botId:
            bot.purchaseChange(message.purchaseId);
            break;
        case config.REDIS_PREFIX+'csgo-purchase-change-'+botId:
            bot.purchaseChange(message.purchaseId);
            break;
        case config.REDIS_PREFIX+'widthdraw-csgo-'+botId:
            bot.widthdrawCsgo(message.accountId, message.token, message.drop);
            break;
        case config.REDIS_PREFIX+'once-free-'+botId:
            bot.getSteamLevel(message.steam_id, message.user_id);
            break;
    }
});
