var Mysql   = require('mysql');
var Config  = require('./Config.js');
var Logging = require('./Logging.js');

var config  = new Config;
var logging = Logging('Db');

var Db = function () {
    logging && console.log('[Db]');

    this.queryCallsQueue = [];

    this.connection = undefined;

    this.connect();
};

Db.prototype.connect = function () {
    logging && console.log('[Db][connect]');

    if (this.connection) {
        delete this.connection;
    }

    var connection = Mysql.createConnection({
        host:               config.DB_HOST,
        port:               config.DB_PORT,
        socketPath:         config.DB_UNIX_SOCKET,
        user:               config.DB_USERNAME,
        password:           config.DB_PASSWORD,
        database:           config.DB_DATABASE,
        supportBigNumbers:  true,
        multipleStatements: true
    });

    connection.connect((function(err) {
        if (err) {
            console.warn('[Db][connect] ' + JSON.stringify(err));
            this.connection = undefined;
            setTimeout(this.connect.bind(this), 1000);
            return;
        }
        logging && console.log('[Db][connect] Connection ' + connection.threadId);
        this.connection = connection;
        while (this.queryCallsQueue.length) {
            logging && console.log('[Db][connect] Query calls queue length is ' + this.queryCallsQueue.length + ', so need reduce');
            var queryCall = this.queryCallsQueue[0];
            this.query(queryCall.query, queryCall.parameterOne, queryCall.parameterTwo);
            this.queryCallsQueue.splice(0, 1);
            logging && console.log('[Db][connect] Query calls queue length is reduced to ' + this.queryCallsQueue.length);
        }
    }).bind(this));

    connection.on('error', (function (err) {
        console.warn('[Db][connect] ' + JSON.stringify(err));
        this.connection = undefined;
        setTimeout(this.connect.bind(this), 1000);
    }).bind(this));
};

Db.prototype.query = function (query, parameterOne, parameterTwo) {
    if (this.connection) {
        this.connection.query(query, parameterOne, parameterTwo);
    } else {
        logging && console.log('[Db][query] Query call queueing');
        this.queryCallsQueue.push({query: query, parameterOne: parameterOne, parameterTwo: parameterTwo});
    }
};

module.exports = Db;
