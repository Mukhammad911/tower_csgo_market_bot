var Request   = require('request');
var WebSocket = require('ws');
var Logging   = require('./Logging.js');
var Db        = require('./Db.js');
var Helper    = require('./Helper.js');

var webSocket = undefined;
var logging   = Logging('Csgotm');
var db        = new Db;
var bot       = undefined;

var Csgotm = function (_bot) {
    logging && console.log('[Csgotm]');

    bot = _bot;

    this.apiKey = undefined;

    this.requestThrootleDuration = 334;
    this.requestLastStamp = undefined;
    this.requestCallsQueue = [];

    this.subscribers = [];

    this.connect();
};

Csgotm.prototype.request = function (data, callback) {
    logging && console.log('[Csgotm][request] ' + data);
    if ( ! this.apiKey) {
        logging && console.log('[Csgotm][request] ' + data + ' request queueing');
        this.requestCallsQueue.push({data: data, callback: callback});
        return;
    }
    if (this.requestLastStamp && (Date.now() - this.requestLastStamp < this.requestThrootleDuration)) {
        logging && console.log('[Csgotm][request] ' + data + ' request queueing');
        this.requestCallsQueue.push({data: data, callback: callback});
        return;
    }
    this.requestLastStamp = Date.now();
    Request(
        (data.indexOf('http') !== 0) ? ('https://csgo.tm/api/' + data + '/?key=' + this.apiKey) : data,
        (function (err, response, body) {
            setTimeout(this.handleRequestCallsQueue.bind(this), this.requestThrootleDuration);
            if (err) {
                console.warn('[Csgotm][request] Request ' + data + ' error: ' + JSON.stringify(err));
                callback(err);
                return;
            }
            if (response.statusCode != 200) {
                console.warn('[Csgotm][request] Request ' + data + ' error: ' + JSON.stringify(response.statusCode));
                callback(response.statusCode);
                return;
            }
            try {
                var result = JSON.parse(body);
            } catch (e) {
                console.warn('[Csgotm][request] Request ' + data + ' response body parse: ' + JSON.stringify(e));
                callback(e);
                return;
            }
            if ( ! result) {
                console.warn('[Csgotm][request] Request ' + data + ' wrong result: ' + JSON.stringify(result));
                callback('Empty result');
                return;
            }
            logging && console.log('[Csgotm][request] Request ' + data + ' completed');
            callback(null, result);
        }).bind(this));
};

Csgotm.prototype.handleRequestCallsQueue = function () {
    if (this.requestLastStamp && (Date.now() - this.requestLastStamp < this.requestThrootleDuration)) {
        return;
    }
    if (this.requestCallsQueue.length) {
        logging && console.log('[Csgotm][handleRequestCallsQueue] requestCallsQueue.length: ' + this.requestCallsQueue.length);
        var requestCall = this.requestCallsQueue[0];
        this.request(requestCall.data, requestCall.callback);
        this.requestCallsQueue.splice(0, 1);
        logging && console.log('[Csgotm][handleRequestCallsQueue] requestCallsQueue.length: ' + this.requestCallsQueue.length);
    }
};

Csgotm.prototype.connect = function () {
    logging && console.log('[Csgotm][connect]');
    db.query('SELECT `api_key` FROM `bots` WHERE `id` = ' + bot.id + ' LIMIT 1;', (function (err, result) {
        if (err) throw err;
        this.apiKey = result[0].api_key;
        this.request('GetWSAuth', (function (err, result) {
            if (err) {
                console.warn('[Csgotm][connect] Request Get wsAuth error: ' + String(err));
                setTimeout(this.connect.bind(this), 60000);
                return;
            }
            if (( ! result.success) || ( ! result.wsAuth)) {
                console.warn('[Csgotm][connect] Request Get wsAuth wrong result: ' + JSON.stringify(result));
                setTimeout(this.connect.bind(this), 60000);
                return;
            }
            logging && console.log('[Csgotm][connect] Request wsAuth result: ' + JSON.stringify(result.wsAuth));
            webSocket = new WebSocket('wss://wsn.dota2.net/wsn/');
            webSocket.on('open', function () {
                logging && console.log('[Csgotm][connect] webSocket.opened');
                webSocket.send(result.wsAuth);
                webSocket.send('newitems');
                webSocket.send('additem_go');
                webSocket.send('itemout_new_go');
                webSocket.send('itemstatus_go');
            });
            webSocket.on('close', (function (code, message) {
                console.warn('[Csgotm][connect] webSocket.closed: ' + code + ', ' + message);
                setTimeout(this.connect.bind(this), 60000);
            }).bind(this));
            webSocket.on('error', (function () {
                console.warn('[Csgotm][connect] webSocket.error');
                webSocket.close();
            }).bind(this));
            webSocket.on('message', (function (message) {
                try {
                    message = JSON.parse(message);
                    var data = JSON.parse(message.data);
                } catch (e) {
                    console.warn('[Csgotm][connect] webSocket.message parse error: ' + JSON.stringify(e));
                    return;
                }
                this.subscribers.forEach(function (callback) {
                    callback(message.type, data);
                });
            }).bind(this));
        }).bind(this));
    }).bind(this));
};

Csgotm.prototype.subscribe = function (callback) {
    logging && console.log('[Csgotm][subscribe]');

    this.subscribers.push(callback);
};

Csgotm.prototype.buy = function (newItem, callback) {
    logging && console.log('[Csgotm][buy] ' + newItem.i_market_hash_name + ' for ' + newItem.ui_price);
    var price = parseInt(newItem.ui_price * 100);
    var hash = '';
    this.request('Buy/' + newItem.i_classid + '_' + newItem.i_instanceid + '/' + price + '/' + hash, (function (err, result) {
        if (err) {
            console.warn('[Csgotm][buy] Request Buy error: ' + String(err));
            if (callback) {
                callback(err);
            }
            return;
        }
        if ((result.result != 'ok') || ( ! result.id)) {
            console.warn('[Csgotm][buy] Request Buy message: ' + result.result);
            if (callback) {
                callback(result.result);
            }
            return;
        }
        logging && console.log('[Csgotm][buy] Request Buy result: ' + JSON.stringify(result));
        if (callback) {
            callback(null, result);
        }
    }).bind(this));
};

Csgotm.prototype.find = function (query, callback) {
    logging && console.log('[Csgotm][find] ' + query);
    var limit = 1;
    this.request('https://csgo.tm/ajax/price/'+encodeURIComponent(query.rarity)+'/'+encodeURIComponent(query.quality)+'/all/1/' + limit + '/'+query.min+';'+query.max+'/' + encodeURIComponent(query.name) + '/all/all', callback);
};

module.exports = Csgotm;