var Steam                             = require('steam');
var SteamWebLogOn                     = require('steam-weblogon');
var SteamTradeOffers                  = require('steam-tradeoffers');
var SteamWeb                          = require('steam-web');
var SteamWebApiKey                    = require('steam-web-api-key');
var SteamCommunityMobileConfirmations = require('steamcommunity-mobile-confirmations');
var SteamTotp                         = require('steam-totp');
var Logging                           = require('./Logging.js');
var Db                                = require('./Db.js');
var Helper                            = require('./Helper.js');
var BotOffersManager                  = require('./BotOffersManager.js');
var BotDropManager                    = require('./BotDropManager.js');
var BotAvailableProductsManager       = require('./BotAvailableProductsManager.js');
var Csgotm                            = require('./Csgotm.js');
var BotPurchaseManager                = require('./BotPurchaseManager.js');
var CsgotmSubscriber                  = require('./CsgotmSubscriber.js');
var Redis                             = require('ioredis');
var Config                            = require('./Config.js');

var steamClient                       = undefined;
var steamUser                         = undefined;
var steamWebLogOn                     = undefined;
var steamFriends                      = undefined;
var steamOffers                       = new SteamTradeOffers;
var steamCommunityMobileConfirmations = undefined;
var logging                           = Logging('Bot');
var db                                = new Db;
var botOffersManager                  = undefined;
var botDropManager                    = undefined;
var botAvailableProductsManager       = undefined;
var csgotm                            = undefined;
var botPurchaseManager                = undefined;
var csgotmSubscriber                  = undefined;
var redis   						  = new Redis;
var config                            = new Config;

var Bot = function (botId) {
    logging && console.log('[Bot]');

    this.id = botId;

    this.steamOffersRequestsQueue = [];

    this.connected = false;
    this.connecting = false;
    this.connect();

    this.confirming = false;
    setTimeout(this.confirm.bind(this), 60000);
    setInterval(this.confirm.bind(this), 1800000);

    botDropManager              = new BotDropManager(this);
    botAvailableProductsManager = new BotAvailableProductsManager(this);
    botOffersManager            = new BotOffersManager(this, botAvailableProductsManager);
    csgotm                      = new Csgotm(this);
    botPurchaseManager          = new BotPurchaseManager(this, csgotm);
    csgotmSubscriber            = new CsgotmSubscriber(this, csgotm, botPurchaseManager, botAvailableProductsManager);
};

Bot.prototype.connect = function () {
    if (this.connecting) {
        return;
    }
    if (this.connected) {
        this.disconnect();
    }
    this.connecting = true;

    logging && console.log('[Bot][connect]');

    db.query('\
		SELECT \
			`steam_login`, \
			`steam_password`, \
			`secret`, \
			`identity_secret`, \
			`device_id`, \
			"" \
		FROM `bots` \
		WHERE `id` = "' + this.id + '"; \
	', (function (err, result) {
        if (err) throw err;

        var botAuthInfo = result[0];

        steamClient   = new Steam.SteamClient();
        steamUser     = new Steam.SteamUser(steamClient);
        steamWebLogOn = new SteamWebLogOn(steamClient, steamUser);
        steamFriends  = new Steam.SteamFriends(steamClient);

        steamClient.on('debug', function (message) {
            logging && console.log('[Bot][connect] steamClient.debug: ' + String(message));
        });
        steamClient.on('error', (function (err) {
            console.warn('[Bot][connect] steamClient.error: ' + String(err));
            this.disconnect();
            setTimeout(this.connect.bind(this), 60000);
            logging && console.log('[Bot][connect] setTimeout connect 60000');
        }).bind(this));
        steamClient.on('loggedOff', function () {
            console.warn('[Bot][connect] steamClient.loggedOff');
        });
        steamClient.connect();

        steamClient.on('connected', function () {
            steamUser.logOn({
                account_name:    botAuthInfo.steam_login,
                password:        botAuthInfo.steam_password,
                two_factor_code: SteamTotp.generateAuthCode(botAuthInfo.secret),
            });
        });

        steamClient.on('logOnResponse', (function (response) {
            if (response.eresult != Steam.EResult.OK) {
                console.warn('[Bot][connect] steamClient.logOnResponse error: ' + JSON.stringify(response));
                db.query('UPDATE `bots` SET `status_code` = "2", `status_msg` = "' + Helper.enumerationValueToString(Steam.EResult, response.eresult) + '" WHERE `id` = "' + this.id + '";');
                this.connecting = false;
                this.disconnect();
                setTimeout(this.connect.bind(this), 60000);
                logging && console.log('[Bot][connect] setTimeout connect 60000');
                return;
            }

            logging && console.log('[Bot][connect] steamClient.logOnResponse OK: ' + JSON.stringify(response));

            steamWebLogOn.webLogOn((function(webSessionID, cookies) {
                logging && console.log('[Bot][connect] steamClient.webLogOn: ' + JSON.stringify(webSessionID) + ', ' + JSON.stringify(cookies));

                SteamWebApiKey({
                    sessionID: webSessionID,
                    webCookie: cookies
                }, (function(err, webAPIKey) {
                    if (err) {
                        console.warn('[Bot][connect] SteamWebApiKey error: ' + String(err));
                        db.query('UPDATE `bots` SET `status_code` = "4", `status_msg` = "' + String(err) + '" WHERE `id` = "' + this.id + '";');
                        this.connecting = false;
                        this.disconnect();
                        setTimeout(this.connect.bind(this), 60000);
                        logging && console.log('[Bot][connect] setTimeout connect 60000');
                        return;
                    }

                    logging && console.log('[Bot][connect] SteamWebApiKey: ' + JSON.stringify(webAPIKey));

                    steamOffers.setup({
                        sessionID: webSessionID,
                        webCookie: cookies,
                        APIKey:    webAPIKey
                    });

                    steamCommunityMobileConfirmations = new SteamCommunityMobileConfirmations(
                        {
                            steamid:         steamClient.steamID,
                            identity_secret: botAuthInfo.identity_secret,
                            device_id:       botAuthInfo.device_id,
                            webCookie:       cookies
                        });

                    steamWeb = new SteamWeb({
                        apiKey: webAPIKey,
                        format: 'json',
                    });

                    steamFriends.setPersonaState(Steam.EPersonaState.Online);

                    db.query('UPDATE `bots` SET `status_code` = "1", `status_msg` = "" WHERE `id` = "' + this.id + '";');

                    this.connecting = false;
                    this.connected = true;


                    handleCallsQueues();

                }).bind(this));
            }).bind(this));
        }).bind(this));
    }).bind(this));

    var handleCallsQueues = (function () {
        while (this.steamOffersRequestsQueue.length) {
            logging && console.log('[Bot][connect] Steam offers requests queue length is ' + this.steamOffersRequestsQueue.length + ', so need reduce');
            var steamOffersRequest = this.steamOffersRequestsQueue[0];
            this.steamOffersRequest(steamOffersRequest.method, steamOffersRequest.options, steamOffersRequest.callback);
            this.steamOffersRequestsQueue.splice(0, 1);
            logging && console.log('[Bot][connect] Steam offers requests queue length is reduced to ' + this.steamOffersRequestsQueue.length);
        }
    }).bind(this);
};

Bot.prototype.disconnect = function () {
    if (this.connecting) {
        return;
    }
    logging && console.log('[Bot][disconnect]');
    steamClient.disconnect();
    this.connected = false;
};

Bot.prototype.confirm = function () {
    if ( ! this.connected) {
        return;
    }
    if (this.confirming) {
        return;
    }

    this.confirming = true;

    steamCommunityMobileConfirmations.FetchConfirmations((function (err, confirmations) {
        if (err) {
            console.warn('[Bot][confirm] FetchConfirmations error: ' + JSON.stringify(err));
            this.confirming = false;
            return;
        }
        logging && console.log('[Bot][confirm] FetchConfirmations received ' + confirmations.length + ' confirmations');
        if ( ! confirmations.length) {
            this.confirming = false;
            return;
        }
        steamCommunityMobileConfirmations.AcceptConfirmation(confirmations[0], (function (err, result) {
            if (err) {
                console.warn('[Bot][confirm] AcceptConfirmation error: ' + JSON.stringify(err));
                this.confirming = false;
                return;
            }
            logging && console.log('[Bot][confirm] AcceptConfirmation result: ' + JSON.stringify(result));
            this.confirming = false;
            if (result && (confirmations.length > 1)) {
                setTimeout(this.confirm.bind(this), 0);
            }
        }).bind(this));
    }).bind(this));
};

Bot.prototype.steamOffersRequest = function (method, options, callback) {
    logging && console.log('[Bot][steamOffersRequest] ' + method);
    if ( ! this.connected) {
        logging && console.log('[Bot][steamOffersRequest] ' + method + ' request queueing');
        this.steamOffersRequestsQueue.push({method: method, options: options, callback: callback});
        return;
    }
    logging && console.log('[Bot][steamOffersRequest] ' + method + '(' + JSON.stringify(options) + ')');
    steamOffers[method](options, (function (err, result) {
        if (err) {
            logging && console.log('[Bot][steamOffersRequest] ' + method + ' error: ' + String(err));
            if (callback) {
                callback(err);
            }
            if (String(err) == 'Error: 401') {
                this.connect();
            }
            return;
        }
        switch (method) {
            case 'makeOffer':
                logging && console.log('[Bot][steamOffersRequest] ' + method + ' result: ' + JSON.stringify(result));
                this.confirm();
                break;
            case 'loadMyInventory':
                logging && console.log('[Bot][steamOffersRequest] ' + method + ' result length: ' + JSON.stringify(result.length));
                break;
            case 'getOffer':
                logging && console.log('[Bot][steamOffersRequest] ' + method + ' result: ' + JSON.stringify((result && result.response && result.response.offer) ? Helper.enumerationValueToString(Helper.ETradeOfferState, result.response.offer.trade_offer_state) : result));
                break;
            case 'getOffers':
                logging && console.log('[Bot][steamOffersRequest] ' + method + ' result: ' + JSON.stringify((result && result.response) ? {sent: result.response.trade_offers_sent ? result.response.trade_offers_sent.length : result.response.trade_offers_sent, received: result.response.trade_offers_received ? result.response.trade_offers_received.length : result.response.trade_offers_received} : result));
                break;
            case 'getItems':
                logging && console.log('[Bot][steamOffersRequest] ' + method + ' result length: ' + JSON.stringify(result.length));
                break;
            case 'cancelOffer':
            case 'acceptOffer':
            default:
                logging && console.log('[Bot][steamOffersRequest] ' + method + ' result: ' + JSON.stringify(result));
        }
        if (callback) {
            callback(null, result);
        }
    }).bind(this));
};

Bot.prototype.sendDrop = function (accountId, token, drop) {
    logging && console.log('[Bot][sendDrop] ' + accountId + ' ' + token + ' ' + drop.id);

    botDropManager.send(accountId, token, drop);
};

Bot.prototype.purchaseChange = function (purchaseId) {
    logging && console.log('[Bot][purchaseChange] ' + purchaseId);

    botPurchaseManager.change(purchaseId);
};

Bot.prototype.getSteamLevel = function(steam_id, user_id){
    logging && console.log('[Bot][getSteamLevel] [userId ' + user_id + ' [steam_id ' + steam_id);
    steamFriends.getSteamLevel([steam_id], function(response, error){
        logging && console.log('[Bot][getSteamLevel] [getSteamLevel] [Intro]');
        var level = 0;
        Object.keys(response).forEach(function(key, value){
            logging && console.log('[Bot][getSteamLevel] [Level]' + response[key]);
            level = parseInt(response[key]);
        });

        if(level >= 4){
            logging && console.log('[Bot][getSteamLevel] [getSteamLevel] [more 4]');
            this.getBalance(user_id, function(balance){
                var updatedBalance = parseFloat(balance) + 0.5;
                this.saveUpdatedBalance(updatedBalance, user_id, function (response) {
                    if(response){
                        logging && console.log('[Bot][getSteamLevel] [SuccessAddedBalance 0.5] [userId]' + user_id + ' [steam_id]' + steam_id);
                        this.updatedUserTableOnceFree(user_id,function(res){
                            if(res){
                                redis.publish(config.REDIS_PREFIX+'once-free',
                                    JSON.stringify({
                                        userId:  user_id,
                                        status:  "success",
                                        message: "Successfully added 0.5$"
                                    }));
                            }
                        }.bind(this));
                    }
                }.bind(this));
            }.bind(this));
        }
        else{
            logging && console.log('[Bot][getSteamLevel] [getSteamLevel] [less 4]');
            redis.publish(config.REDIS_PREFIX+'once-free',
                JSON.stringify({
                    userId:  user_id,
                    status:  "error",
                    message: "The steam level must be more 4 level or equal"
                }));
        }
    }.bind(this));
}

Bot.prototype.saveUpdatedBalance = function (summ, user_id, callback) {
    logging && console.log('[Bot][saveUpdatedBalance][UserID]' + user_id);
    db.query('\
     UPDATE `balance` \
     SET \
     `summ` = ' + summ + ' \
     WHERE (`user_id` = ' + user_id + ') \
     and (`type` = "0") \
     LIMIT 1; \
     ', function (err, result) {
        if (err) throw err;
        callback(true);
    }.bind(this));
}

Bot.prototype.getBalance = function (user_id, callback) {
    logging && console.log('[Bot][getBalance][UserID]' + user_id);

    db.query('SELECT * FROM `balance` WHERE type = 0 and `user_id` = ' + user_id + ';', (function (err, result) {
        if (err) throw err;
        callback(result[0].summ)
    }).bind(this));
}

Bot.prototype.updatedUserTableOnceFree = function (user_id, callback) {
    logging && console.log('[Bot][updatedUserTableOnceFree][UserID]' + user_id);

    logging && console.log('[Bot][saveUpdatedBalance][UserID]' + user_id);
    db.query('\
     UPDATE `users` \
     SET \
     `once_free` = 1 \
     WHERE (`id` = ' + user_id + ') \
     LIMIT 1; \
     ', function (err, result) {
        if (err) throw err;
        callback(true);
    }.bind(this));
}

module.exports = Bot;
