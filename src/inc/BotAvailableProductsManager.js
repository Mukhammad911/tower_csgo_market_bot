var Logging = require('./Logging.js');
var Db      = require('./Db.js');
var Helper  = require('./Helper.js');

var logging = Logging('BotAvailableProductsManager');
var db      = new Db;
var bot     = undefined;

var BotAvailableProductsManager = function (_bot) {
    logging && console.log('[BotAvailableProductsManager]');

    bot = _bot;

    this.syncing = false;
    setTimeout(this.sync.bind(this), 0);
    setInterval(this.sync.bind(this), 3600000);
};

BotAvailableProductsManager.prototype.sync = function () {
    if (this.syncing) {
        return;
    }
    this.syncing = true;
    logging && console.log('[BotAvailableProductsManager][sync]');
    logging && console.log('[BotAvailableProductsManager][sync] bot.steamOffersRequest loadMyInventory');
    bot.steamOffersRequest('loadMyInventory', {
        appId:        730,
        contextId:    String(2),
        tradableOnly: true
    }, (function (err, result){
        if (err) {
            console.warn('[BotAvailableProductsManager][sync] bot.steamOffersRequest loadMyInventory error: ' + String(err));
            this.syncing = false;
            return;
        }
        logging && console.log('[BotAvailableProductsManager][sync] bot.steamOffersRequest loadMyInventory result length: ' + JSON.stringify(result.length));
        var items = result;
        var itemsAssets = items.map(function (item) {
            return item.id;
        });
        db.query('\
				(SELECT `assetid` FROM `available_products` WHERE `bot_id` = ' + bot.id + ') \
				UNION ALL \
				(SELECT `assetid` FROM `drops` WHERE (`bot_id` = ' + bot.id + ') AND (`status` NOT IN ("sold", "accepted"))); \
			', (function (err, result) {
            if (err) throw err;

            var existsAssets = result.map(function (row) {
                return String(row.assetid);
            });
            var newItems = items.filter(function (item) {
                return (existsAssets.indexOf(item.id) === -1);
            });
            var oldAssets = existsAssets.filter(function (assetid) {
                return (itemsAssets.indexOf(assetid) === -1);
            });
            this.syncing = false;

            if (oldAssets.length) {
                logging && console.log('[BotAvailableProductsManager][sync] Removing oldAssets: ' + JSON.stringify(oldAssets));
                db.query('DELETE FROM `available_products` WHERE (`bot_id` = ' + bot.id + ') AND (`assetid` IN (?)); ', [oldAssets], (function (err, result)
                {
                    if (err) throw err;
                    logging && console.log('[BotAvailableProductsManager][sync] Removing oldAssets result: ' + JSON.stringify(result));
                }).bind(this));
            }
            this.savePendingAvailableProducts(Helper.pendingAvailableProductsByItems(newItems));
        }).bind(this));
    }).bind(this));
};

BotAvailableProductsManager.prototype.savePendingAvailableProducts = function (pendingAvailableProducts) {
    logging && console.log('[BotAvailableProductsManager][savePendingAvailableProducts] ' + pendingAvailableProducts.length);
    pendingAvailableProducts.forEach((function (availableProduct) {
        if (
            ( ! availableProduct.name)              ||
            ( ! availableProduct.short_description) ||
            ( ! availableProduct.class)
        ) {
            console.warn('[BotAvailableProductsManager][savePendingAvailableProducts] Can not save pendingAvailableProduct: ' + JSON.stringify(availableProduct));
            return;
        }
        db.query('\
			SELECT \
				`products`.`id`, \
				`products`.`price`, \
				(SELECT `csgotm_items`.`price` FROM `csgotm_items` WHERE (`csgotm_items`.`assetid` = "' + Helper.escape(availableProduct.assetid) + '") LIMIT 1) AS `csgotm_item_price` \
			FROM `products` \
			WHERE ( \
				(`products`.`name`= "' + Helper.escape(availableProduct.name) + '") OR \
				(`products`.`name`= "' + Helper.escape(availableProduct.name).replace('★ ', '') + '") \
			) \
			AND (`products`.`short_description` = "' + Helper.escape(availableProduct.short_description) + '") \
			AND (`products`.`class` = "' + Helper.escape(availableProduct.class) + '") \
			LIMIT 1; \
		', (function (err, result){
            if (err) throw err;
            if ( ! result.length) {
                console.warn('[BotAvailableProductsManager][savePendingAvailableProducts] Can not save pendingAvailableProduct: ' + JSON.stringify(availableProduct));
                return;
            }
            availableProduct.product_id = result[0].id;
            availableProduct.price      = result[0].csgotm_item_price ? (result[0].csgotm_item_price * 100) : result[0].price;
            availableProduct.bot_id     = bot.id;
            this.save(availableProduct);
        }).bind(this));
    }).bind(this));
};

BotAvailableProductsManager.prototype.save = function (availableProduct) {
    logging && console.log('[BotAvailableProductsManager][save] ' + availableProduct.market_name);
    db.query('\
		START TRANSACTION; \
		DELETE FROM `available_products` WHERE `assetid` = "' + availableProduct.assetid + '"; \
		INSERT INTO `available_products` SET ?; \
		COMMIT; \
	', availableProduct, (function (err, result){
        if (err) throw err;
        logging && console.log('[BotAvailableProductsManager][save] Result: ' + JSON.stringify(result));
    }).bind(this));
};

module.exports = BotAvailableProductsManager;
