var Redis   = require('ioredis');
var Config  = require('./Config.js');
var Logging = require('./Logging.js');
var Db      = require('./Db.js');
var Helper  = require('./Helper.js');

var redis   = new Redis;
var config  = new Config;
var logging = Logging('BotOffersManager');
var db      = new Db;
var bot     = undefined;
var botAvailableProductsManager = undefined;

var BotOffersManager = function (_bot, _botAvailableProductsManager) {
    logging && console.log('[BotOffersManager]');

    bot = _bot;
    botAvailableProductsManager = _botAvailableProductsManager;

    this.activeOffersDropsIds = {};

    this.checking = false;
    this.checkingTimeHistoricalCutoff = Math.round(Date.now() / 1000);
    setTimeout(this.check.bind(this), 0);
    setInterval(this.check.bind(this), 60000);

    this.checkingActiveOffersDrops = false;
    setInterval(this.checkActiveOffersDrops.bind(this), 1000);

    this.checkingOfferedDrops = false;
    setTimeout(this.checkOfferedDrops.bind(this), 0);
    setInterval(this.checkOfferedDrops.bind(this), 300000);
};

BotOffersManager.prototype.check = function () {
    if (this.checking) {
        return;
    }
    this.checking = true;

    logging && console.log('[BotOffersManager][check]');

    var checkingTimeHistoricalCutoff = this.checkingTimeHistoricalCutoff;
    var newCheckingTimeHistoricalCutoff = Math.round(Date.now() / 1000);

    logging && console.log('[BotOffersManager][check] bot.steamOffersRequest getOffers');
    bot.steamOffersRequest('getOffers', {
        get_sent_offers:        1,
        get_received_offers:    1,
        active_only:            1,
        time_historical_cutoff: checkingTimeHistoricalCutoff,
    }, (function (err, result) {
        if (err) {
            console.warn('[BotOffersManager][check] bot.steamOffersRequest getOffers error: ' + String(err));
            this.checking = false;
            return;
        }

        logging && console.log('[BotOffersManager][check] bot.steamOffersRequest getOffers result: ' + JSON.stringify((result && result.response) ? {sent: result.response.trade_offers_sent ? result.response.trade_offers_sent.length : result.response.trade_offers_sent, received: result.response.trade_offers_received ? result.response.trade_offers_received.length : result.response.trade_offers_received} : result));
        this.checkingTimeHistoricalCutoff = newCheckingTimeHistoricalCutoff;
        if (result && result.response && result.response.trade_offers_sent) {
            result.response.trade_offers_sent.forEach(this.handle.bind(this));
        }
        if (result && result.response && result.response.trade_offers_received) {
            result.response.trade_offers_received.forEach(this.handle.bind(this));
        }
        this.checking = false;
    }).bind(this));
};

BotOffersManager.prototype.handle = function (offer) {
    if (('string' == typeof offer) || ('number' == typeof offer)) {
        var offerId = offer;
        logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest getOffer ' + offerId);
        bot.steamOffersRequest('getOffer', { tradeOfferId: offerId }, (function (err, result) {
            if (err) {
                console.warn('[BotOffersManager][handle] bot.steamOffersRequest getOffer ' + offerId + ' error: ' + String(err));
                return;
            }
            if (( ! result) || ( ! result.response) || ( ! result.response.offer)) {
                console.warn('[BotOffersManager][handle] bot.steamOffersRequest getOffer ' + offerId + ' bad result: ' + JSON.stringify(result));
                return;
            }
            this.handle(result.response.offer);
        }).bind(this));
        return;
    }

    var offerInfoString = offer.tradeofferid + ' (' + (offer.is_our_offer ? 'sent' : 'received') + ', ' + Helper.enumerationValueToString(Helper.ETradeOfferState, offer.trade_offer_state) + ')';
    logging && console.log('[BotOffersManager][handle] ' + offerInfoString);

    if (
        offer.is_our_offer &&
        (offer.trade_offer_state == Helper.ETradeOfferState.Active)
    ) {
        if (Date.now() / 1000 - offer.time_created > 5 * 60) {
            logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest cancelOffer ' + offerInfoString + ', now: ' + (Date.now() / 1000) + ', time_created: ' + offer.time_created);
            bot.steamOffersRequest('cancelOffer', { tradeOfferId: offer.tradeofferid }, (function (err, result){
                if (err) {
                    console.warn('[BotOffersManager][handle] bot.steamOffersRequest cancelOffer ' + offerInfoString + ' error: ' + String(err));
                    return;
                }
                logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest cancelOffer ' + offerInfoString + ' result: ' + JSON.stringify(result));
                this.handle(offer.tradeofferid);
            }).bind(this));
            return;
        }
        db.query('\
			SELECT `drops_market`.`id` \
			FROM `drops_market` \
			WHERE (`bot_id` = ' + bot.id + ') \
			AND (`offer_id` = "' + offer.tradeofferid + '") \
			AND (`status` = "offered") \
			LIMIT 1; \
		', (function (err, result) {
            if (err) throw err;
            if (result.length) {
                this.activeOffersDropsIds[offer.tradeofferid] = result[0].id;
                return;
            }
            logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest cancelOffer handle_bool true ' + offerInfoString);
            bot.steamOffersRequest('cancelOffer', { tradeOfferId: offer.tradeofferid });
        }).bind(this));
    }
    console.log('State from steam offer',offer.trade_offer_state);
    if (offer.trade_offer_state == Helper.ETradeOfferState.CreatedNeedsConfirmation) {
        bot.confirm();
    }

    if (
        offer.is_our_offer &&
        ([Helper.ETradeOfferState.InvalidItems, Helper.ETradeOfferState.Declined, Helper.ETradeOfferState.Canceled, Helper.ETradeOfferState.Expired, Helper.ETradeOfferState.Countered].indexOf(offer.trade_offer_state) !== -1)
    ) {
        logging && console.log('[BotOffersManager][handle] Notactive ' + offerInfoString);
        delete this.activeOffersDropsIds[offer.tradeofferid];
        db.query('\
			UPDATE `drops_market` \
			SET \
				`status` = "pending" \
			WHERE (`bot_id` = ' + bot.id + ') \
			AND (`offer_id` = "' + offer.tradeofferid + '") \
			AND (`status` = "offered") \
		', function (err, result){
            if (err) throw err;
            redis.publish(config.REDIS_PREFIX+'bot',
                JSON.stringify({
                    userId:  offer.steamid_other,
                    status:  "declined",
                    offerId: offer.tradeofferid,
                }));
        });
    }

    if (
        offer.is_our_offer &&
        (offer.trade_offer_state == Helper.ETradeOfferState.Accepted)
    ) {
        logging && console.log('[BotOffersManager][handle] Accepted ' + offerInfoString);
        delete this.activeOffersDropsIds[offer.tradeofferid];
        db.query('\
			UPDATE `drops_market` \
			SET \
				`status` = "accepted" \
			WHERE (`bot_id` = ' + bot.id + ') \
			AND (`offer_id` = "' + offer.tradeofferid + '") \
			AND (`status` = "offered") \
			LIMIT 1; \
		',
            function (err, result) {
                if (err) throw err;
                redis.publish(config.REDIS_PREFIX+'bot',
                    JSON.stringify({
                        userId:  offer.steamid_other,
                        status:  "accepted",
                        offerId: offer.tradeofferid,
                    }));
            });
    }

    if (
        ( ! offer.is_our_offer) &&
        (offer.trade_offer_state == Helper.ETradeOfferState.Active)
    ) {
        if (offer.items_to_receive && offer.items_to_receive.length && ( ! offer.items_to_give)) {
            if ( ! offer.message) {
                logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest acceptOffer ' + offerInfoString);
                bot.steamOffersRequest('acceptOffer', { tradeOfferId: offer.tradeofferid }, (function (err, result) {
                    if (err) {
                        console.warn('[BotOffersManager][handle] ' + JSON.stringify(offer));
                        console.warn('[BotOffersManager][handle] bot.steamOffersRequest acceptOffer error: ' + String(err));
                        return;
                    }
                    logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest acceptOffer result: ' + JSON.stringify(result));
                    var tradeid = result.tradeid;
                    bot.steamOffersRequest('getItems', {tradeId: tradeid}, (function (err, result) {
                        if (err) {
                            console.warn('[BotOffersManager][handle] bot.steamOffersRequest getItems error: ' + String(err));
                            return;
                        }
                        logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest getItems result length: ' + JSON.stringify(result.length));
                        botAvailableProductsManager.savePendingAvailableProducts(Helper.pendingAvailableProductsByItems(result));
                    }).bind(this));
                }).bind(this));
            }
        } else {
            logging && console.log('[BotOffersManager][handle] bot.steamOffersRequest declineOffer ' + offerInfoString);
            bot.steamOffersRequest('declineOffer', { tradeOfferId: offer.tradeofferid });
        }
    }
};

BotOffersManager.prototype.checkActiveOffersDrops = function () {
    if (this.checkingActiveOffersDrops) {
        return;
    }
    this.checkingActiveOffersDrops = true;
    var ids = [];
    for (var tradeofferid in this.activeOffersDropsIds) {
        ids.push(this.activeOffersDropsIds[tradeofferid]);
    }
    if ( ! ids.length) {
        this.checkingActiveOffersDrops = false;
        return;
    }
    logging && console.log('[BotOffersManager][checkActiveOffersDrops] ' + JSON.stringify(ids));
    db.query('\
		SELECT `drops_market`.`id` \
		FROM `drops_market` \
		WHERE (`bot_id` = ' + bot.id + ') \
		AND (`id` IN (' + ids.join(',') + ')) \
		AND (`status` = "offered") \
		AND TRUE; \
	', (function (err, result) {
        if (err) throw err;
        var ids = result.map(function (row) {
            return row.id;
        });
        for (var tradeofferid in this.activeOffersDropsIds) {
            if (ids.indexOf(this.activeOffersDropsIds[tradeofferid]) === -1) {
                logging && console.log('[BotOffersManager][checkActiveOffersDrops] bot.steamOffersRequest cancelOffer checkActiveDropBool true ' + tradeofferid);
                bot.steamOffersRequest('cancelOffer', { tradeOfferId: tradeofferid });
            }
        }
        logging && console.log('[BotOffersManager][checkActiveOffersDrops] Completed ' + JSON.stringify(ids));
        this.checkingActiveOffersDrops = false;
    }).bind(this));
};

BotOffersManager.prototype.checkOfferedDrops = function ()
{
    if (this.checkingOfferedDrops)
    {
        return;
    }
    this.checkingOfferedDrops = true;
    db.query('\
		SELECT `drops_market`.`offer_id` \
		FROM `drops_market` \
		WHERE (`bot_id` = ' + bot.id + ') \
		AND (`status` = "offered") \
		AND TRUE; \
	',
        (function (err, result)
        {
            if (err) throw err;
            result.forEach((function (row)
            {
                this.handle(row.offer_id);
            }).bind(this));
            this.checkingOfferedDrops = false;
        }).bind(this));
};

module.exports = BotOffersManager;
