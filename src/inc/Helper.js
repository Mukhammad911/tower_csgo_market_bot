var Steam  = require('steam');
var Helper = module.exports;

module.exports.enumerationValueToString = function (enumeration, value) {
    for (var enumerationValueString in enumeration) {
        if (enumeration[enumerationValueString] === value) {
            return enumerationValueString;
        }
    }
};

module.exports.classOfItem = function (item) {
    if (item.tags) {
        for (var tagIndex in item.tags) {
            var tag = item.tags[tagIndex];

            if ((tag.category == "Type") && (tag.category_name == "Type") && (tag.internal_name == "CSGO_Type_Knife")) {
                return 'knifes';
            }

            if ((tag.category == "Rarity") && (tag.category_name == "Quality")) {
                switch (tag.internal_name) {
                    case 'Rarity_Rare':
                    case 'Rarity_Rare_Weapon':
                        return 'mil-spec';
                    case 'Rarity_Mythical':
                    case 'Rarity_Mythical_Weapon':
                        return 'restricted';
                    case 'Rarity_Legendary':
                    case 'Rarity_Legendary_Weapon':
                        return 'classified';
                    case 'Rarity_Contraband':
                    case 'Rarity_Ancient_Weapon':
                        return 'covert';
                    case 'Rarity_Common':
                    case 'Rarity_Common_Weapon':
                        return 'common';
                    case 'Rarity_Uncommon':
                    case 'Rarity_Uncommon_Weapon':
                        return 'uncommon';
                    case 'Rarity_Ancient':
                        return 'extraordinary';
                }
            }
        }
    }

    console.warn('[Helper][classOfItem] Can not determine item class: ' + JSON.stringify(item));

    return undefined;
};

module.exports.stattrakOfItem = function (item) {
    if (item.tags) {
        for (var tagIndex in item.tags) {
            var tag = item.tags[tagIndex];

            if ((tag.category == "Quality") && (tag.category_name == "Category") && (tag.internal_name == "strange")) {
                return true;
            }
        }
    }

    return false;
};

module.exports.parseMarketName = function (market_name) {
    market_name = market_name
        .replace('StatTrak™ ', '')
        .replace('StatTrak™', '')
        .replace('Souvenir', '')
        .trim();

    var split1 = market_name.split('|');
    var split2 = (split1.length > 1) ? split1[1].split('(') : undefined;

    return {
        name:              split1[0].trim().toLowerCase(),
        short_description: split2 ? split2[0].trim().toLowerCase() : undefined,
        quality:           split2 && (split2.length > 1) ? split2[1].replace(')', '').trim() : ''
    }
};

module.exports.buildMarketName = function (name, short_description, quality, stattrak)
{
    return (stattrak ? 'StatTrak™ ' : '') + name + ' | ' + short_description + (quality ? [' (', quality, ')'].join('') : '');
};

module.exports.escape = function (value) {
    return String(value).replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
};

module.exports.ETradeOfferState = {
    Invalid:                  1, // Invalid
    Active:                   2, // This trade offer has been sent, neither party has acted on it yet.
    Accepted:                 3, // The trade offer was accepted by the recipient and items were exchanged.
    Countered:                4, // The recipient made a counter offer
    Expired:                  5, // The trade offer was not accepted before the expiration date
    Canceled:                 6, // The sender cancelled the offer
    Declined:                 7, // The recipient declined the offer
    InvalidItems:             8, // Some of the items in the offer are no longer available (indicated by the missing flag in the output)
    CreatedNeedsConfirmation: 9, // The offer hasn't been sent yet and is awaiting email/mobile confirmation. The offer is only visible to the sender.
    CanceledBySecondFactor:   10, // Either party canceled the offer via email/mobile. The offer is visible to both parties, even if the sender canceled it before it was sent.
    InEscrow:                 11 // The trade has been placed on hold. The items involved in the trade have all been removed from both parties' inventories and will be automatically delivered in the future.
};

module.exports.pendingAvailableProductsByItems = function (items) {
    return items.map(function (item) {
        return {
            name:              Helper.parseMarketName(item.market_name).name,
            short_description: Helper.parseMarketName(item.market_name).short_description,
            quality:           Helper.parseMarketName(item.market_name).quality,
            stattrak:          Helper.stattrakOfItem(item),
            class:             Helper.classOfItem(item),
            product_id:        undefined,
            price:             undefined,
            image_hash:        item.icon_url,
            market_name:       item.market_name,
            created_at:        new Date,
            updated_at:        new Date,
            assetid:           item.id,
            classid:           item.classid,
            instanceid:        item.instanceid,
            bot_id:            undefined
        };
    });
};

module.exports.EPurchaseQuota = {
    Low:  0,
    Mid:  1,
    High: 2
};
