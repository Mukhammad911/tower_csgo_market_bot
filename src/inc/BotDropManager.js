var Redis   = require('ioredis');
var Config  = require('./Config.js');
var Logging = require('./Logging.js');
var Db      = require('./Db.js');
var Helper  = require('./Helper.js');

var redis   = new Redis;
var config  = new Config;
var logging = Logging('BotDropManager');
var db      = new Db;
var bot     = undefined;

var BotDropManager = function (_bot) {
    logging && console.log('[BotDropManager]');

    bot = _bot;

    this.sendCallsQueue = [];

    this.sending = false;

    this.checking = false;
    this.checkingTimeHistoricalCutoff = Math.round(Date.now() / 1000);

    db.query('\
		SELECT \
			(SELECT `steamid` FROM `users` WHERE `users`.`id` = `drops_market`.`user_id`) AS `steamid`, \
			(SELECT `token` FROM `users` WHERE `users`.`id` = `drops_market`.`user_id`) AS `token`, \
			`drops_market`.* \
		FROM `drops_market` \
		WHERE (`bot_id` = ' + bot.id + ') \
		AND (`status` = "requested"); \
	', (function (err, result) {
        if (err) throw err;

        for (var result_index in result) {
            var steamid = result[result_index].steamid;
            var token = result[result_index].token;
            delete result[result_index].steamid;
            delete result[result_index].token;
            var drop = result[result_index];
            this.send(steamid, token, drop);

        }


    }).bind(this));
};

BotDropManager.prototype.send = function (accountId, token, drop) {
    logging && console.log('[BotDropManager][send] ' + accountId + ' ' + token + ' ' + drop.id);

    if (this.sending) {
        logging && console.log('[BotDropManager][send] Send call queueing');
        this.sendCallsQueue.push({accountId: accountId, token: token, drop: drop});
    } else {
        this.sending = true;

        logging && console.log('[BotDropManager][send] bot.steamOffersRequest makeOffer ' + JSON.stringify(drop));

        bot.steamOffersRequest('makeOffer', {
            partnerSteamId: accountId,
            accessToken:    token,
            itemsFromMe:    [{
                appid:     730,
                contextid: String(2),
                amount:    1,
                assetid:   String(drop.assetid)
            }],
            itemsFromThem:  [],
            message:        config.OFFER_MESSAGE
        }, (function (err, result) {
            if (err) {
                console.warn('[BotDropManager][send] ' + JSON.stringify(drop));
                console.warn('[BotDropManager][send] bot.steamOffersRequest makeOffer error: ' + String(err));
                var error = String(err);
                db.query('\
						UPDATE `drops_market` \
						SET \
							`status` = "pending" \
						WHERE `id` = ' + drop.id + '; \
					', function (err, result) {
                    if (err) throw err;
                    redis.publish(config.REDIS_PREFIX+'bot',
                        JSON.stringify({
                            userId:  accountId,
                            dropId:  drop.id,
                            status:  "error",
                            message: error
                        }));
                    sendComplete();
                });
                return;
            }

            logging && console.log('[BotDropManager][send] bot.steamOffersRequest makeOffer result: ' + JSON.stringify(result));
            var tradeofferid = result.tradeofferid;
            db.query('\
					UPDATE `drops_market` \
					SET \
						`status` = "offered", \
						`offer_id` = "' + tradeofferid + '" \
					WHERE `id` = ' + drop.id + '; \
				', function (err, result) {
                if (err) throw err;
                redis.publish(config.REDIS_PREFIX+'bot',
                    JSON.stringify({
                        userId:  accountId,
                        dropId:  drop.id,
                        status:  "offered",
                        offerId: tradeofferid
                    }));
                sendComplete();
            });
        }).bind(this));
    }

    var sendComplete = (function () {
        this.sending = false;

        if (this.sendCallsQueue.length) {
            logging && console.log('[BotManager][send] Send calls queue length is ' + this.sendCallsQueue.length + ', so need reduce');
            var sendCall = this.sendCallsQueue[0];
            this.send(sendCall.accountId, sendCall.token, sendCall.drop);
            this.sendCallsQueue.splice(0, 1);
            logging && console.log('[BotManager][send] Send calls queue length is reduced to ' + this.sendCallsQueue.length);
        }
    }).bind(this);
};



BotDropManager.prototype.sendWidthdrawCsgo = function (accountId, token, drop) {
    logging && console.log('[BotDropManager][sendWidthdrawCsgo] ' + accountId + ' ' + token + ' ' + drop);

    if (this.sending) {
        logging && console.log('[BotDropManager][sendWidthdrawCsgo] Send call queueing');
        this.sendCallsQueue.push({accountId: accountId, token: token, drop: drop});
    } else {
        this.sending = true;

        logging && console.log('[BotDropManager][sendWidthdrawCsgo] bot.steamOffersRequest makeOffer ' + JSON.stringify(drop));

        var itemTrade = [];
        for (var p=0;p<drop.length;p++){
            itemTrade.push({
                appid:     730,
                contextid: String(2),
                amount:    1,
                assetid:   String(drop[p].assetid)
            })
        }
        console.log('itemTrade ', itemTrade);
        bot.steamOffersRequest('makeOffer', {
            partnerSteamId: accountId,
            accessToken:    token,
            itemsFromMe:    itemTrade,
            itemsFromThem:  [],
            message:        config.OFFER_MESSAGE
        }, (function (err, result) {
            if (err) {
                console.warn('[BotDropManager][sendWidthdrawCsgo] ' + JSON.stringify(drop));
                console.warn('[BotDropManager][sendWidthdrawCsgo] bot.steamOffersRequest makeOffer error: ' + String(err));
                var error = String(err);
                for (var j=0;j<drop.length;j++){
                    db.query('\
						UPDATE `drops_market` \
						SET \
							`status` = "pending" \
						WHERE `id` = ' + drop[j].id + '; \
					', function (err, result) {
                        if (err) throw err;

                    });
                    redis.publish(config.REDIS_PREFIX+'bot',
                        JSON.stringify({
                            userId:  accountId,
                            dropId:  drop[j].id,
                            status:  "error",
                            message: error
                        }));
                    sendComplete();
                }
                return;
            }

            setTimeout((function () {
                this.check();
            }).bind(this),4000);

            setTimeout((function () {

                logging && console.log('[BotDropManager][sendWidthdrawCsgo] bot.steamOffersRequest makeOffer result: ' + JSON.stringify(result));
                var tradeofferid = result.tradeofferid;
                for (var k=0;k<drop.length;k++){
                    db.query('\
                        UPDATE `drops_market` \
                        SET \
                            `status` = "offered", \
                            `offer_id` = "' + tradeofferid + '" \
                        WHERE `id` = ' + drop[k].id + '; \
                    ', function (err, result) {
                        if (err) throw err;

                        console.log(result);

                    });
                }
                redis.publish(config.REDIS_PREFIX+'bot',
                    JSON.stringify({
                        userId:  accountId,
                        status:  "offered",
                        offerId: tradeofferid,
                        bot_id: bot.id
                    }));
                sendComplete();

            }).bind(this), 6000);


        }).bind(this));
    }

    var sendComplete = (function () {
        this.sending = false;

        if (this.sendCallsQueue.length) {
            logging && console.log('[BotManager][sendWidthdrawCsgo] Send calls queue length is ' + this.sendCallsQueue.length + ', so need reduce');
            var sendCall = this.sendCallsQueue[0];
            this.sendWidthdrawCsgo(sendCall.accountId, sendCall.token, sendCall.drop);
            this.sendCallsQueue.splice(0, 1);
            logging && console.log('[BotManager][sendWidthdrawCsgo] Send calls queue length is reduced to ' + this.sendCallsQueue.length);
        }
    }).bind(this);
};

BotDropManager.prototype.check = function () {
    if (this.checking) {
        return;
    }
    this.checking = true;

    logging && console.log('[BotDropManager][check]');

    var checkingTimeHistoricalCutoff = this.checkingTimeHistoricalCutoff;
    var newCheckingTimeHistoricalCutoff = Math.round(Date.now() / 1000);

    logging && console.log('[BotDropManager][check] bot.steamOffersRequest getOffers');
    bot.steamOffersRequest('getOffers', {
        get_sent_offers:        1,
        get_received_offers:    1,
        active_only:            1,
        time_historical_cutoff: checkingTimeHistoricalCutoff,
    }, (function (err, result) {
        if (err) {
            console.warn('[BotDropManager][check] bot.steamOffersRequest getOffers error: ' + String(err));
            this.checking = false;
            return;
        }

        logging && console.log('[BotDropManager][check] bot.steamOffersRequest getOffers result: ' + JSON.stringify((result && result.response) ? {sent: result.response.trade_offers_sent ? result.response.trade_offers_sent.length : result.response.trade_offers_sent, received: result.response.trade_offers_received ? result.response.trade_offers_received.length : result.response.trade_offers_received} : result));
        this.checkingTimeHistoricalCutoff = newCheckingTimeHistoricalCutoff;
        if (result && result.response && result.response.trade_offers_sent) {
            result.response.trade_offers_sent.forEach(this.handle.bind(this));
        }
        if (result && result.response && result.response.trade_offers_received) {
            result.response.trade_offers_received.forEach(this.handle.bind(this));
        }
        this.checking = false;
    }).bind(this));
};

BotDropManager.prototype.handle = function (offer) {
    if (('string' == typeof offer) || ('number' == typeof offer)) {
        var offerId = offer;
        logging && console.log('[BotDropManager][handle] bot.steamOffersRequest getOffer ' + offerId);
        bot.steamOffersRequest('getOffer', { tradeOfferId: offerId }, (function (err, result) {
            if (err) {
                console.warn('[BotDropManager][handle] bot.steamOffersRequest getOffer ' + offerId + ' error: ' + String(err));
                return;
            }
            if (( ! result) || ( ! result.response) || ( ! result.response.offer)) {
                console.warn('[BotDropManager][handle] bot.steamOffersRequest getOffer ' + offerId + ' bad result: ' + JSON.stringify(result));
                return;
            }
            this.handle(result.response.offer);
        }).bind(this));
        return;
    }

    var offerInfoString = offer.tradeofferid + ' (' + (offer.is_our_offer ? 'sent' : 'received') + ', ' + Helper.enumerationValueToString(Helper.ETradeOfferState, offer.trade_offer_state) + ')';
    logging && console.log('[BotDropManager][handle] ' + offerInfoString);


    if (offer.trade_offer_state == Helper.ETradeOfferState.CreatedNeedsConfirmation) {
        bot.confirm();
    }
};

module.exports = BotDropManager;
