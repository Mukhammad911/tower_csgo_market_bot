var ChildProcess = require('child_process');
var Logging      = require('./Logging.js');
var Db           = require('./Db.js');

var fork    = ChildProcess.fork;
var logging = Logging('BotManager');
var db      = new Db;

var BotManager = function () {
    logging && console.log('[BotManager]');

    this.bots = {};

    db.query('SELECT `id` FROM `bots` WHERE `enabled` = 1;', (function (err, result) {
        if (err) throw err;

        for (var result_index in result) {
            this.enableBot(result[result_index].id);
        }
    }).bind(this));
};

BotManager.prototype.disableBot = function (botId) {
    logging && console.log('[BotManager][disableBot] ' + botId);

    try {
        this.bots[botId].kill();
        delete this.bots[botId];
    } catch (e) {
        console.warn('[BotManager][disableBot] ' + JSON.stringify(e));
    }
};

BotManager.prototype.enableBot = function (botId) {
    if (this.bots[botId] !== undefined) {
        this.disableBot(botId);
    }

    logging && console.log('[BotManager][enableBot] ' + botId);

    this.bots[botId] = fork('./bot', [botId], {});
};

module.exports = BotManager;
