var DotEnv = require('dotenv');

var Config = function () {
    return require('util')._extend(DotEnv.config(), DotEnv.config({path: DotEnv.config().PATH}));
};

module.exports = Config;
