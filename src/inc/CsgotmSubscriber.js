var Logging            = require('./Logging.js');
var Db                 = require('./Db.js');
var Helper             = require('./Helper.js');
var CsgotmItemsManager = require('./CsgotmItemsManager.js');

var logging                     = Logging('CsgotmSubscriber');
var db                          = new Db;
var bot                         = undefined;
var csgotm                      = undefined;
var botPurchaseManager          = undefined;
var botAvailableProductsManager = undefined;
var csgotmItemsManager          = new CsgotmItemsManager;

var CsgotmSubscriber = function (_bot, _csgotm, _botPurchaseManager, _botAvailableProductsManager) {
    logging && console.log('[CsgotmSubscriber]');

    bot                         = _bot;
    csgotm                      = _csgotm;
    botPurchaseManager          = _botPurchaseManager;
    botAvailableProductsManager = _botAvailableProductsManager;

    this.itemRequestThrootleDuration = 30000;
    this.itemRequestLastStamp = undefined;
    this.itemRequestCallsQueue = [];

    csgotm.subscribe(this.handle.bind(this));

    this.checking = false;
    setTimeout(this.check.bind(this), 0);
    setInterval(this.check.bind(this), 150000);
};

CsgotmSubscriber.prototype.check = function () {
    if (this.checking) {
        return;
    }
    logging && console.log('[CsgotmSubscriber][check]');
    this.checking = true;
    var checkRequestsCount = 2;
    var checkRequestsCompletedCount = 0;
    csgotm.request('Trades', (function (err, result) {
        if (err) {
            console.warn('[CsgotmSubscriber][check] Request Trades error: ' + String(err));
            checkComplete();
            return;
        }
        if (result.length === undefined) {
            console.warn('[CsgotmSubscriber][check] Request Trades wrong result: ' + JSON.stringify(result));
            checkComplete();
            return;
        }
        logging && console.log('[CsgotmSubscriber][check] Request Trades result length: ' + JSON.stringify(result.length));
        var botIds = [];
        result.forEach(function (item) {
            if ((item.ui_status == 4) && (botIds.indexOf(item.ui_bid) === -1)) {
                logging && console.log('[CsgotmSubscriber][check] Request Trades item ui_status ' + JSON.stringify(item));
                csgotmItemsManager.updateOneByBuyedConditions({
                    bot_id:             bot.id,
                    status:             'buyed',
                    csgotm_purchase_id: item.ui_id
                }, {
                    status:        'stored',
                    csgotm_bot_id: item.ui_bid
                });
                botIds.push(item.ui_bid);
            }
        });
        botIds.forEach((function (botId) {
            this.itemRequest(botId);
        }).bind(this));
        checkComplete();
    }).bind(this));
    csgotm.request('MarketTrades', (function (err, result) {
        if (err) {
            console.warn('[CsgotmSubscriber][check] Request Market Trades error: ' + String(err));
            checkComplete();
            return;
        }
        logging && console.log('[CsgotmSubscriber][check] Request Market Trades result: ' + JSON.stringify(result));
        checkComplete();
    }).bind(this));
    var checkComplete = (function () {
        if (++checkRequestsCompletedCount == checkRequestsCount) {
            logging && console.log('[CsgotmSubscriber][check] Completed');
            this.checking = false;
        }
    }).bind(this);
};

CsgotmSubscriber.prototype.handle = function (type, data) {
    switch (type) {
        case 'newitem':
            if (botPurchaseManager.isSuitableNewItem(data)) {
                logging && console.log('[CsgotmSubscriber][handle] Buy ' + JSON.stringify(data));
                csgotm.buy(data);
            }
            break;

        case 'additem_go':
            break;

        case 'itemout_new_go':
            break;

        case 'itemstatus_go':
            if (data.status == 4) {
                logging && console.log('[CsgotmSubscriber][handle] itemstatus_go ' + JSON.stringify(data));
                csgotmItemsManager.updateOneByBuyedConditions({
                    bot_id:             bot.id,
                    status:             'buyed',
                    csgotm_purchase_id: data.id
                }, {
                    status:        'stored',
                    csgotm_bot_id: data.bid
                });
                this.itemRequest(data.bid);
            }
            break;
    }
};

CsgotmSubscriber.prototype.itemRequest = function (botId) {
    logging && console.log('[CsgotmSubscriber][itemRequest] ' + botId);
    if (this.itemRequestLastStamp && (Date.now() - this.itemRequestLastStamp < this.itemRequestThrootleDuration)) {
        var itemRequestCallAlreadyQueued = false;
        for (var queue_index in this.itemRequestCallsQueue) {
            if (this.itemRequestCallsQueue[queue_index].botId == botId) {
                itemRequestCallAlreadyQueued = true;
                break;
            }
        }
        if ( ! itemRequestCallAlreadyQueued) {
            logging && console.log('[CsgotmSubscriber][itemRequest] ' + botId + ' item request queueing');
            this.itemRequestCallsQueue.push({botId: botId});
        }
        return;
    }
    this.itemRequestLastStamp = Date.now();
    logging && console.log('[CsgotmSubscriber][itemRequest] ' + botId + ' csgotm.request');
    csgotm.request('ItemRequest/out/' + botId, (function (err, result) {
        setTimeout(this.handleItemRequestCallsQueue.bind(this), this.itemRequestThrootleDuration - (Date.now() - this.itemRequestLastStamp));
        if (err) {
            console.warn('[CsgotmSubscriber][itemRequest] Request Item Request error: ' + String(err));
            return;
        }
        if ( ! result.success) {
            console.warn('[CsgotmSubscriber][itemRequest] Request Item Request error: ' + JSON.stringify(result.error));
            return;
        }
        logging && console.log('[CsgotmSubscriber][itemRequest] Request Item Request result: ' + JSON.stringify(result));
        this.itemRequestAcceptOffer(botId, result.trade);
    }).bind(this));
};

CsgotmSubscriber.prototype.itemRequestAcceptOffer = function (csgotmBotId, tradeofferid, attempt) {
    var maxAttempts = 3;
    attempt = attempt || 1;
    logging && console.log('[CsgotmSubscriber][itemRequestAcceptOffer] ' + tradeofferid + ' (attempt ' + attempt + ')');
    logging && console.log('[CsgotmSubscriber][itemRequestAcceptOffer] bot.steamOffersRequest acceptOffer');
    bot.steamOffersRequest('acceptOffer', {tradeOfferId: tradeofferid}, (function (err, result) {
        if (err) {
            console.warn('[CsgotmSubscriber][itemRequestAcceptOffer] bot.steamOffersRequest acceptOffer error: ' + String(err));
            if (attempt < maxAttempts) {
                setTimeout((function () {
                    this.itemRequestAcceptOffer(csgotmBotId, tradeofferid, attempt + 1);
                }).bind(this), 1000);
            }
            return;
        }
        logging && console.log('[CsgotmSubscriber][itemRequestAcceptOffer] bot.steamOffersRequest acceptOffer result: ' + JSON.stringify(result));
        this.itemRequestLastStamp = Date.now() - this.itemRequestThrootleDuration;
        setTimeout(this.handleItemRequestCallsQueue.bind(this), 0);
        var tradeid = result.tradeid;
        bot.steamOffersRequest('getItems', {tradeId: tradeid}, (function (err, result) {
            if (err) {
                console.warn('[CsgotmSubscriber][itemRequestAcceptOffer] bot.steamOffersRequest getItems error: ' + String(err));
                return;
            }
            logging && console.log('[CsgotmSubscriber][itemRequestAcceptOffer] bot.steamOffersRequest getItems result length: ' + JSON.stringify(result.length));
            var csgotmItemsUpdateCount = 0;
            var afterCsgotmItemsUpdate = function () {
                if (++csgotmItemsUpdateCount == result.length) {
                    botAvailableProductsManager.savePendingAvailableProducts(Helper.pendingAvailableProductsByItems(result));
                }
            };
            result.forEach((function (item) {
                csgotmItemsManager.updateOneByStoredConditions({
                    bot_id:        bot.id,
                    status:        'stored',
                    classid:       item.classid,
                    instanceid:    item.instanceid,
                    csgotm_bot_id: csgotmBotId
                }, {
                    status:       'received',
                    tradeofferid: tradeofferid,
                    tradeid:      tradeid,
                    assetid:      item.id
                }, afterCsgotmItemsUpdate);
            }).bind(this));
        }).bind(this));
    }).bind(this));
};

CsgotmSubscriber.prototype.handleItemRequestCallsQueue = function () {
    if (this.itemRequestLastStamp && (Date.now() - this.itemRequestLastStamp < this.requestThrootleDuration)) {
        return;
    }
    if (this.itemRequestCallsQueue.length) {
        logging && console.log('[CsgotmSubscriber][handleItemRequestCallsQueue] itemRequestCallsQueue.length: ' + this.itemRequestCallsQueue.length);
        var itemRequestCall = this.itemRequestCallsQueue[0];
        this.itemRequest(itemRequestCall.botId);
        this.itemRequestCallsQueue.splice(0, 1);
        logging && console.log('[CsgotmSubscriber][handleItemRequestCallsQueue] itemRequestCallsQueue.length: ' + this.itemRequestCallsQueue.length);
    }
};

module.exports = CsgotmSubscriber;
