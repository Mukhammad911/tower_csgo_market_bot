var Logging = require('./Logging.js');
var Db = require('./Db.js');
var Helper = require('./Helper.js');
var CsgotmItemsManager = require('./CsgotmItemsManager.js');

var logging = Logging('BotPurchaseManager');
var db = new Db;
var bot = undefined;
var csgotm = undefined;
var csgotmItemsManager = new CsgotmItemsManager;

var csgotmQualities = {
    '(Поношенное)': 'Well-Worn',
    '(После полевых испытаний)': 'Field-Tested',
    '(Закаленное в боях)': 'Battle-Scarred',
    '(Немного поношенное)': 'Minimal Wear',
    '(Прямо с завода)': 'Factory New'
};
var priceMultipliers = {
    '(Закаленное в боях)': 0.8,
    '(Поношенное)': 1,
    '(После полевых испытаний)': 1.2,
    '(Немного поношенное)': 1.3,
    '(Прямо с завода)': 1.4
};
var priceMultipliersStattrak = {
    '(Закаленное в боях)': 1,
    '(Поношенное)': 1,
    '(После полевых испытаний)': 1.2,
    '(Немного поношенное)': 1.6,
    '(Прямо с завода)': 1.8
};


var BotPurchaseManager = function (_bot, _csgotm) {
    logging && console.log('[BotPurchaseManager]');
    bot = _bot;
    csgotm = _csgotm;

    this.handlingThrootleDuration = 5000;
    this.new_arr = [];
    this.quality = '';
    this.rarity = '';
    this.min = '';
    this.max = '';
    this.my_arr = [];
    this.count = [];
    this.buy_item = [];
    this.purchase = undefined;


    db.query('\
		SELECT \
			`csgo_purchases`.`id` \
		FROM `csgo_purchases` \
		WHERE (`bot_id` = ' + bot.id + ') \
		LIMIT 1; \
	', (function (err, result) {
        if (err) throw err;
        if (result.length) {
            this.change(result[0].id);
        }
    }).bind(this));
};
BotPurchaseManager.prototype.change = function (purchaseId) {
    this.handlingThrootleDuration = 5000;
    this.new_arr = [];
    this.quality = '';
    this.rarity = '';
    this.min = '';
    this.max = '';
    this.my_arr = [];
    this.count = [];
    this.buy_item = [];
    this.purchase = undefined;

    this.endData(purchaseId, (function () {
        this.secondProccess(this.purchase, (function () {
            this.getItems(this.my_arr, (function () {
            }).bind(this));
        }).bind(this));
    }).bind(this));
};

BotPurchaseManager.prototype.change_out = function () {
    this.handlingThrootleDuration = 5000;
    this.new_arr = [];
    this.quality = '';
    this.rarity = '';
    this.min = '';
    this.max = '';
    this.my_arr = [];
    this.count = [];
    this.buy_item = [];
    this.purchase = undefined;


    db.query('\
		SELECT \
			`csgo_purchases`.`id` \
		FROM `csgo_purchases` \
		WHERE (`bot_id` = ' + bot.id + ') \
		LIMIT 1; \
	', (function (err, result) {
        if (err) throw err;

        console.log(result);

        if (result.length) {
            this.change(result[0].id);
        }
    }).bind(this));
};
BotPurchaseManager.prototype.endData = function (purchaseId, callback) {
    db.query('\
		SELECT \
			`csgo_purchases`.* \
		FROM `csgo_purchases` \
		WHERE (`id` = ' + purchaseId + ') \
		LIMIT 1; \
	', (function (err, result) {
        if (err) throw err;
        if (!result.length) {
            this.new_arr = [];
            this.quality = '';
            this.rarity = '';
            this.min = '';
            this.max = '';
            this.my_arr = [];
            this.count = [];
            this.buy_item = [];
            this.purchase = undefined;

            logging && console.log('[BotPurchaseManager][load] Not found ' + purchaseId);
            return;
        }
        else {
            console.log(result);

            this.purchase = result[0];
            this.quality = result[0].quality;
            this.rarity = result[0].rarity;
            this.min = result[0].min_price;
            this.max = result[0].max_price;
            callback();
        }
    }).bind(this));
};
BotPurchaseManager.prototype.secondProccess = function (result, callback) {
    var purchase = result;

    console.log("Purchase id " + purchase.id);

    db.query('\
	        	SELECT \
			`csgo_search_items`.* \
		     FROM `csgo_search_items` \
		     WHERE (`purchase_id` = ' + purchase.id + ') AND TRUE', (function (err, result_t) {

        console.log(result_t);

        for (var i = 0; i < result_t.length; i++) {
            this.new_arr.push({
                product_name: result_t[i].name,
                product_quota: result_t[i].quota,
                product_min_price: this.min,
                product_max_price: this.max,
                product_quality: this.quality,
                product_rarity: this.rarity,
                product_price_product: result_t[i].price,
            })
        }
        this.my_arr = this.new_arr;
        callback();
    }).bind(this));

};
BotPurchaseManager.prototype.getItems = function (my_arr) {
    var all_arr = my_arr;
    var new_arr2 = my_arr;
    this.count = [];

    console.log(my_arr);

    this.getCount(all_arr, new_arr2, (function () {
    }).bind(this));
};
BotPurchaseManager.prototype.getCount = function (all_arr, new_arr2) {
    var last_all = all_arr;
    var all_items = new_arr2.slice(0);
    var simple_item = last_all[0];
    if (typeof simple_item == 'undefined') {
        this.sixProcess(all_items, (function () {
            this.sevenProcess(function () {
            }.bind(this))
        }).bind(this));
    }
    else {
        last_all.splice(0, 1);
        db.query('SELECT \
        COUNT(`available_products`.`id`) AS `count_available_products` \
        FROM `available_products` \
        WHERE (`available_products`.`name` = "' + simple_item.product_name + '") \
        AND TRUE \
        UNION ALL \
        SELECT \
        COUNT(`drops_market`.`id`) AS `count_drops` \
        FROM `drops_market` \
        WHERE (`drops_market`.`bot_id` = "' + simple_item.product_name + '") \
        AND (`drops_market`.`status` = "pending"); \
		    ', (function (err, result) {
            if (err) throw err;
            var dota_available_product_count = result[0].count_available_products;
            var dota_drop_count =   result[1].count_available_products;
            var summ_count = dota_available_product_count + dota_drop_count;

            console.log(dota_available_product_count);

            this.count.push(summ_count);
            this.getCount(last_all, all_items, (function () {
            }).bind(this));
        }).bind(this));
    }
};
BotPurchaseManager.prototype.sevenProcess = function () {
    var buy_item = this.buy_item;
    var queryHandleStep = (function () {
        var that = this;
        setTimeout((function () {
            var those = that;
            if (!buy_item.length) {
                this.buy_item = [];
                this.new_arr = [];
                this.my_arr = [];
                this.count = [];
                setTimeout((function () {
                    if (those.purchase) {
                        those.change(those.purchase.id);
                    }
                }).bind(this), 1200000);
            }
            else {
                var query = buy_item[0];
                buy_item.splice(0, 1);
                csgotm.find(query, (function (err, result) {
                    if (err) {
                        queryHandleStep();
                        return;
                    }
                    logging && console.log('[BotPurchaseManager][handle] csgotm.find result: ' + JSON.stringify(result));
                    if (( !result.length) || ( !result[0].length)) {

                        console.log('Enter here and returned');
                        queryHandleStep();
                        return;
                    }
                    for (var resultIndex in result[0]) {
                        if (resultIndex > 0) {
                            queryHandleStep();
                            break;
                        }

                        if (!result[0][resultIndex].length) {
                            queryHandleStep();
                            break;
                        }

                        var item = {
                            i_market_hash_name: result[0][resultIndex][2],
                            i_classid: result[0][resultIndex][0],
                            i_instanceid: result[0][resultIndex][1],
                            ui_price: result[0][resultIndex][3],
                        };
                        var itemPrice = item.ui_price * 100;
                        var productPrice = query.price_product * 100;
                        var priceMultiplier = ((query.name == 'sticker') ? 1 : this.getPriceMultiplier(item.i_market_hash_name));
                        if (itemPrice < productPrice * priceMultiplier) {
                            logging && console.log('[BotPurchaseManager][handle] ' + query + ' (' + item.i_market_hash_name + '): ProductPrice: ' + (productPrice / 100) + ', priceMultiplier: ' + priceMultiplier + ', ItemPrice: ' + item.ui_price);
                            logging && console.log('[BotPurchaseManager][handle] csgotm.buy ' + query + ': ' + JSON.stringify(item));
                            csgotm.buy(item, (function (err, result) {
                                if (err) {
                                    setTimeout((function () {
                                        queryHandleStep();
                                    }).bind(this), this.handlingThrootleDuration);
                                    return;
                                }
                                csgotmItemsManager.save({
                                    bot_id: bot.id,
                                    status: 'buyed',
                                    classid: item.i_classid,
                                    instanceid: item.i_instanceid,
                                    price: item.ui_price,
                                    csgotm_purchase_id: result.id
                                });
                            }).bind(this));
                            queryHandleStep();
                        } else {
                            logging && console.log('[BotPurchaseManager][handle] [failed buy cause] ' + query + ' (' + item.i_market_hash_name + '): ProductPrice: ' + (productPrice / 100) + ', priceMultiplier: ' + priceMultiplier + ', ItemPrice: ' + item.ui_price);
                            queryHandleStep();
                        }
                    }
                }).bind(this));
            }
        }).bind(this), 334);
    }).bind(this);
    queryHandleStep();
};
BotPurchaseManager.prototype.sixProcess = function (arr, callback) {

    if (this.purchase != 'undefined') {
        var item = this.count;

        console.log(item);

        console.log(arr);
        if (item.length <= arr.length) {
            console.log('Product quota is true');
            for (var i = 0; i < item.length; i++) {
                if (item[i] >= arr[i].product_quota) {
                }
                else {
                    this.buy_item.push({
                        name: arr[i].product_name,
                        quality: arr[i].product_quality,
                        rarity: arr[i].product_rarity,
                        min: arr[i].product_min_price,
                        max: arr[i].product_max_price,
                        price_product: arr[i].product_price_product
                    });
                }
            }
        } else {
            console.log('Product quota paradox')
        }
        callback();
    }
};

BotPurchaseManager.prototype.getPriceMultiplier = function (i_market_hash_name) {
    var priceMultiplier = 0;
    for (var csgotmQuality in priceMultipliers) {
        if (i_market_hash_name.indexOf(csgotmQuality) !== -1) {
            priceMultiplier = priceMultipliers[csgotmQuality];
            break;
        }
    }
    for (var csgotmQuality in priceMultipliersStattrak) {
        if ((i_market_hash_name.indexOf('StatTrak™') !== -1) && (i_market_hash_name.indexOf(csgotmQuality) !== -1)) {
            priceMultiplier = priceMultipliersStattrak[csgotmQuality];
            break;
        }
    }
    return priceMultiplier;
};

module.exports = BotPurchaseManager;
