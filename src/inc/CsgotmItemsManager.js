var Logging = require('./Logging.js');
var Db      = require('./Db.js');
var Helper  = require('./Helper.js');

var logging = Logging('CsgotmItemsManager');
var db      = new Db;

var CsgotmItemsManager = function () {
    logging && console.log('[CsgotmItemsManager]');
};

CsgotmItemsManager.prototype.save = function (csgotmItem, callback) {
    logging && console.log('[CsgotmItemsManager][save] ' + JSON.stringify(csgotmItem));
    csgotmItem.created_at = new Date;
    csgotmItem.updated_at = new Date;
    db.query('\
		DELETE FROM `csgotm_items` WHERE (`assetid` IS NULL) AND (NOW() - INTERVAL 24 HOUR > `csgotm_items`.`updated_at`); \
		INSERT INTO `csgotm_items` SET ?; \
	', csgotmItem, (function (err, result) {
        if (err) throw err;
        logging && console.log('[CsgotmItemsManager][save] Result: ' + JSON.stringify(result));
        if (callback) {
            callback();
        }
    }).bind(this));
};

CsgotmItemsManager.prototype.updateOneByBuyedConditions = function (conditions, data, callback) {
    logging && console.log('[CsgotmItemsManager][updateOneByBuyedConditions] ' + JSON.stringify(conditions) + ', ' + JSON.stringify(data));
    data.updated_at = new Date;
    db.query('\
		UPDATE `csgotm_items` \
		SET ? \
		WHERE (`bot_id` = ' + conditions.bot_id + ') \
		AND (`status` = "' + conditions.status + '") \
		AND (`csgotm_purchase_id` = "' + conditions.csgotm_purchase_id + '") \
		LIMIT 1;\
	', data, (function (err, result) {
        if (err) throw err;
        logging && console.log('[CsgotmItemsManager][updateOneByBuyedConditions] Result: ' + JSON.stringify(result));
        if (callback) {
            callback();
        }
    }).bind(this));
};

CsgotmItemsManager.prototype.updateOneByStoredConditions = function (conditions, data, callback) {
    logging && console.log('[CsgotmItemsManager][updateOneByStoredConditions] ' + JSON.stringify(conditions) + ', ' + JSON.stringify(data));
    data.updated_at = new Date;
    db.query('\
		UPDATE `csgotm_items` \
		SET ? \
		WHERE (`bot_id` = ' + conditions.bot_id + ') \
		AND (`status` = "' + conditions.status + '") \
		AND (`classid` = "' + conditions.classid + '") \
		AND (`instanceid` = "' + conditions.instanceid + '") \
		AND (`csgotm_bot_id` = "' + conditions.csgotm_bot_id + '") \
		LIMIT 1; \
	', data, (function (err, result) {
        if (err) throw err;
        logging && console.log('[CsgotmItemsManager][updateOneByStoredConditions] Result: ' + JSON.stringify(result));
        if (callback) {
            callback();
        }
    }).bind(this));
};

module.exports = CsgotmItemsManager;
