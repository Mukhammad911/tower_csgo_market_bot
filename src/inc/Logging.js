var Config = require('./Config');
var config = new Config;

var Logging = function (classname) {
    return (((config.LOGGING.split(',').indexOf('*') !== -1) || (config.LOGGING.split(',').indexOf(classname) !== -1)) && (config.LOGGING.split(',').indexOf('!' + classname) === -1));
};

module.exports = Logging;
